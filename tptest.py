from scapy.all import ARP, Ether, srp, IP, TCP, sr, ICMP, sr1, conf

target_ip = "10.33.18.158/24"
# IP Address for the destination
# create ARP packet
arp = ARP(pdst=target_ip)
# create the Ether broadcast packet
# ff:ff:ff:ff:ff:ff MAC address indicates broadcasting
ether = Ether(dst="ff:ff:ff:ff:ff:ff")
# stack them
packet = ether/arp

result = srp(packet, timeout=3)[0]

#a list of clients, we will fill this in the upcoming loop
clients = []

for sent, received in result:
    #for each response, append ip and mac address to `clients` list()
    clients.append({'ip': received.psrc, 'mac': received.hwsrc})

print("Available devices in the network:")
print("IP" + " "*18+"MAC")

for client in clients:  
    print("{:16}    {}".format(client['ip'], client['mac']))


ans, unans = sr( IP(dst="192.168.1.5")
        /TCP(flags="S", dport=(1,1024)), timeout = 1)


for i in ans:
    print(i.answer.getlayer(TCP).dport)
    print(i.answer.getlayer(TCP).flags)






